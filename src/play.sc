
val a: Option[String] = Some("1")
val b = Some("this is b")

val maybeFirstName : Option[String] = Some("Mark")
val maybeSurname : Option[String] = None // Some("ss")

for { firstName <- maybeFirstName
      _ = println(firstName)
      surname <- maybeSurname
      b <- b
} yield (firstName, surname, b)


maybeFirstName.getOrElse("")
