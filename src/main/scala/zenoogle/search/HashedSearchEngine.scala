package zenoogle.search
import zenoogle.Models
import zenoogle.Models._
import cats.implicits._
import scala.io.StdIn


class HashedSearchEngine(
                          dataStore: DataStore,
                          links: Links,
                          searchIndexes: SearchIndexes
                        ) extends SearchEngine {
  type Hash = String

  def search(query: SearchQuery): List[Models.SearchResult] = {
      val SearchQuery(entity, term, value) = query
      val hash = term + value

      val res = entity match {
        case "1" => getUserRelatedItems(hash)
        case "2" => getTicketRelatedItems(hash)
        case "3" => getOrganizationRelatedItems(hash)
        case _ => {
          println("Invalid selection")
          List.empty
        }
      }
      res.length match {
        case 0 => List()
        case _ => res
      }
    }

  def getUserRelatedItems: Hash => List[UserSearchResult] = { hash =>
      val userIds: List[UserId] =
        searchIndexes.userIndex.getOrElse(hash, List())
      val users: List[User] =
        userIds.map(uid => dataStore.userStore.get(uid)).sequence.get
      val results: List[UserSearchResult] = users map { user =>
        val tickets = links.userTickerIdx
          .getOrElse(user._id, List())
          .map(tid => dataStore.ticketStore.get(tid))

        val org = dataStore.orgStore.get(user._id)
        UserSearchResult(user, org, tickets.sequence.get)
      }
      results
    }

  def getTicketRelatedItems: Hash => List[TicketSearchResult] = { hash =>
      val ticketIds: List[TicketId] =
        searchIndexes.ticketIndex.getOrElse(hash, List())
      val tickets: List[Ticket] =
        ticketIds.map(tid => dataStore.ticketStore.get(tid)).sequence.get
      val results: List[TicketSearchResult] = tickets map { ticket =>
        val user = ticket.submitter_id.flatMap(id => dataStore.userStore.get(id))
        val org = dataStore.orgStore(ticket.organization_id.get)
        TicketSearchResult(ticket, user, org)
      }
    results
  }

  def getOrganizationRelatedItems: Hash => List[OrganizationSearchResult] = { hash =>
    val orgIds: List[OrgId] = searchIndexes.orgIndex.getOrElse(hash, List())
    val organizations: List[Organization] =
      orgIds.map(oid => dataStore.orgStore.get(oid)).sequence.get

    val results: List[OrganizationSearchResult] = organizations map { org =>
      val users = links.orgUserIdx.getOrElse(org._id, List())
          .map(uid => dataStore.userStore(uid))

      val tickets = links.orgTicketIdx
        .getOrElse(org._id, List())
        .map(tid => dataStore.ticketStore(tid))
      OrganizationSearchResult(org, users, tickets)
    }
    results
  }
}

object HashedSearchEngine {

  def showHelp(): Unit = {
    val help = s"""
**** Welcome to Zendesk search ****
Type 'quit' to exit at any time, press 'Enter' to continue

    Select search options:
    * Press 1 to search Zendesk
    * Press 2 to view a list of searchable fields
    * Type 'quit' to exit at any time

****************************************
"""
    println(help)
  }

  def searchCommand(): SearchQuery = {
    println("Select 1) Users 2) Tickets or 3) Organisations ")
    val entity = StdIn.readLine()

    println("Enter search term")
    val term = StdIn.readLine()

    println("Enter search value")
    val value = StdIn.readLine()

    // validate searchTerm
    SearchQuery(entity, term, value)
  }

  def showSearchableFields(): String = {
    ""
  }
}
