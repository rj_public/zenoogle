package zenoogle

// import zenoogle.Errors.ErrorOr
import zenoogle.Models._
import zenoogle.DataReader.build
import zenoogle.search.{HashedSearchEngine}
import scala.io.StdIn

object Main extends App {

  val data = DataFiles("organizations.json", "users.json", "tickets.json")

  /**
    *  Recursive fn to get user input and trigger action
    */
  def show(): Unit = {
    HashedSearchEngine.showHelp()
    val cmd = StdIn.readLine()
    cmd match {
      case "1" => {
        val searchQuery: SearchQuery = HashedSearchEngine.searchCommand()
        val list = searchEngine.search(searchQuery)
        val output = list.length match {
          case 0 => "No results found! please try again"
          case _ => list.map(l => l.display())
        }
        println(output)
      }
      case "2"    => HashedSearchEngine.showSearchableFields()
      case _ => System.exit(0)
    }
    show()
  }

  // build data store, links and search indexes
  val (dataStore, links, searchIndexes) = build(data)
  val searchEngine = new HashedSearchEngine(dataStore, links, searchIndexes)

  // Ready to search
  show()
}
