package zenoogle

import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder, Error}
import zenoogle.Models.{OrganizationStore, _}
import io.circe.parser.decode
import zenoogle.search.SearchIndexer

import scala.collection.mutable
import scala.io.Source

object DataReader {
  implicit val orgDecoder: Decoder[Organization] = deriveDecoder
  implicit val orgEncoder: Encoder[Organization] = deriveEncoder
  implicit val userDecoder: Decoder[User] = deriveDecoder
  implicit val userEncoder: Encoder[User] = deriveEncoder
  implicit val ticketDecoder: Decoder[Ticket] = deriveDecoder
  implicit val tickerEncoder: Encoder[Ticket] = deriveEncoder

  /**
    * Load data from json files into hash map
    * @param DataFiles
    * @return DataStore
    */
  def loadData: DataFiles => DataStore = { data =>
    val userHashMap: UserStore = mutable.HashMap.empty
    val orgHashMap: OrganizationStore = mutable.HashMap.empty
    val ticketHashMap: TicketStore = mutable.HashMap.empty

    val orgJson = Source.fromResource(data.orgDataFile).getLines.mkString("\n")
    val userJson = Source.fromResource(data.userDataFile).getLines.mkString("\n")
    val ticketJson = Source.fromResource(data.ticketsDataFile).getLines.mkString("\n")

    val orgsW: Either[Error, List[Organization]] = decode[List[Organization]](orgJson)
    val usersW: Either[Error, List[User]] = decode[List[User]](userJson)
    val ticketW: Either[Error, List[Ticket]] = decode[List[Ticket]](ticketJson)

    (orgsW, usersW, ticketW) match {
      case (Right(organizations), Right(users), Right(tickets)) => {
        organizations.foreach(org => orgHashMap += (org._id -> org))
        users.foreach(user => userHashMap += (user._id -> user))
        tickets.foreach(ticket => ticketHashMap += (ticket._id -> ticket))
      }
      case _ => {
        println("Errored reading file")
      }
    }
    DataStore(orgHashMap, userHashMap, ticketHashMap)
  }


  /**
    * PopulateLinkIndexes generates a Container(Links) HashMap with top down references.
    * eg: orgUserIdx has user indexes for an org_id
    * @return Links top down links
    */
  def populateLinkIndexes: (OrganizationStore, UserStore, TicketStore) => Links = { (_, users, tickets) =>
    val orgTicketIdx: OrgTicketHashTable = mutable.HashMap.empty
    val orgUserIdx: OrgUserHashTable = mutable.HashMap.empty
    val userTicketIdx: UserTicketHashTable = mutable.HashMap.empty

    users foreach { case (k, v) =>
      v.organization_id match {
        case Some(oid) => orgUserIdx += (oid -> List(k)) // TODO append to existing instead of overwriting
        case _ => ()
      }
    }

    tickets foreach { case (k, v) =>
      v.submitter_id match {
        case Some(userId) => {
          // TODO append to existing instead of overwriting
          userTicketIdx += (userId -> List(k))
        }
        case _ => ()
      }
      v.organization_id match {
        case Some(orgId) => orgTicketIdx += (orgId -> List(k))
        case _ => ()
      }
    }

    Links(orgUserIdx, orgTicketIdx, userTicketIdx)
  }

  /**
    * Build an in memory Datastore, RelationIndex and Search Index
    * @param datafiles DataFiles
    * @return Tuple3(DataStore, Links, SearchIndexes)
    */
  def build: DataFiles => ((DataStore, Links, SearchIndexes)) = { data =>
    val dataStore: DataStore = DataReader.loadData(data)
    val DataStore(organizations, users, tickets) = dataStore
    val links = DataReader.populateLinkIndexes(organizations, users, tickets)

    val searchIndex = SearchIndexer.index(organizations, users, tickets)
    (dataStore, links, searchIndex)
  }
}
