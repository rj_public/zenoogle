package zenoogle.search

import zenoogle.BaseSpec
import zenoogle.DataReader.build
import zenoogle.Models._

import scala.language.reflectiveCalls

class HashedSearchEngineSpec extends BaseSpec {
  def fixture =
    new {
      val data = DataFiles("organizations.json", "users.json", "tickets.json")
      val (dataStore, links, searchIndexes) = build(data)
      val searchEngine = new HashedSearchEngine(dataStore, links, searchIndexes)
    }

  "Given a search query for user id 1" should "return 1 result" in {
    val q = SearchQuery("1", "_id", "1")
    val r = fixture.searchEngine.search(q)
    r.length should be (1)
    noException should be thrownBy 0 / 1
  }

  "testShowHelp" should "not throw any errors" in {
    HashedSearchEngine.showHelp()
    noException should be thrownBy 0 / 1
  }
}
