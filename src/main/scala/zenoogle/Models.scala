package zenoogle

import scala.collection.mutable

object Models {

  type OrgId = Int
  type UserId = Int
  type TicketId = String

  type OrganizationStore = mutable.HashMap[OrgId, Organization]
  type UserStore = mutable.HashMap[UserId, User]
  type TicketStore = mutable.HashMap[TicketId, Ticket]

  type OrgTicketHashTable = mutable.HashMap[OrgId, List[TicketId]]
  type OrgUserHashTable = mutable.HashMap[OrgId, List[UserId]]
  type UserTicketHashTable = mutable.HashMap[UserId, List[TicketId]]

  type OrgSearchIndex = mutable.HashMap[String, List[OrgId]]
  type UserSearchIndex = mutable.HashMap[String, List[UserId]]
  type TicketSearchIndex = mutable.HashMap[String, List[TicketId]]

  case class SearchQuery(
      entity: String,
      searchTerm: String,
      searchValue: String
  )

  case class SearchIndexes(
      orgIndex: OrgSearchIndex,
      userIndex: UserSearchIndex,
      ticketIndex: TicketSearchIndex
  )

  case class DataStore(
      orgStore: OrganizationStore,
      userStore: UserStore,
      ticketStore: TicketStore
  )

  case class Links(
      orgUserIdx: OrgUserHashTable,
      orgTicketIdx: OrgTicketHashTable,
      userTickerIdx: UserTicketHashTable
  )

  /**
    * Request/Input models
    */
  case class DataFiles(
      orgDataFile: String,
      userDataFile: String,
      ticketsDataFile: String
  )

  trait BaseEntity {
    def searchableFields: List[String]
  }

  case class Organization(
      _id: OrgId,
      url: String,
      external_id: String,
      name: String,
      domain_names: List[String],
      created_at: String,
      details: String,
      shared_tickets: Boolean,
      tags: List[String]
  ) extends BaseEntity {
    override def searchableFields: List[String] = List(
      "_id",
      "url",
      "external_id",
      "name",
      "domain_names",
      "created_at",
      "details",
      "shared_tickets",
      "tags"
    )
  }

  case class User(
      _id: UserId,
      url: String,
      external_id: String,
      name: String,
      alias: Option[String],
      created_at: String,
      active: Boolean,
      verified: Option[Boolean],
      shared: Option[Boolean],
      locale: Option[String],
      timezone: Option[String],
      last_login_at: Option[String],
      email: Option[String],
      phone: Option[String],
      signature: Option[String],
      organization_id: Option[OrgId],
      tags: List[String],
      suspended: Boolean,
      role: String
  ) extends BaseEntity {
    override def searchableFields: List[String] =
      List(
        "_id",
        "url",
        "external_id",
        "name",
        "alias",
        "created_at",
        "active",
        "verified",
        "shared",
        "locale",
        "timezone",
        "last_login_at",
        "email",
        "phone",
        "signature",
        "organization_id",
        "tags",
        "suspended",
        "role"
      )
  }

  case class Ticket(
      _id: TicketId,
      url: String,
      external_id: String,
      created_at: String,
      `type`: Option[String],
      subject: String,
      description: Option[String],
      priority: String,
      status: String,
      submitter_id: Option[UserId],
      assignee_id: Option[UserId],
      organization_id: Option[OrgId],
      tags: List[String],
      has_incidents: Option[Boolean],
      due_at: Option[String],
      via: Option[String]
  ) extends BaseEntity {
    override def searchableFields: List[String] =
      List(
        "_id",
        "url",
        "external_id",
        "created_at",
        "`type`",
        "subject",
        "description",
        "priority",
        "status",
        "submitter_id",
        "assignee_id",
        "organization_id",
        "tags",
        "has_incidents",
        "due_at",
        "via"
      )
  }

  /**
    * Response/Output models
    */
  trait SearchResult {
    def display(): String
  }
  case class UserSearchResult(
      user: User,
      organization: Option[Organization],
      tickets: List[Ticket]
  ) extends SearchResult {
    val ticketStr = tickets.zipWithIndex
      .map { case (value, index) => s"Ticket_${index}  ${value.description}" }
      .mkString("\n")
    override def display(): String = {
      s"""
          Search result:
          _id           ${user._id}
          url           ${user.url}
         external_id    ${user.external_id}
         name           ${user.name}
         alias          ${user.alias}
         created_at     ${user.created_at}
         active         ${user.active}
         verified       ${user.verified}
         shared         ${user.shared}
         locale         ${user.locale}
         timezone       ${user.timezone}
         last_login_at  ${user.last_login_at}
         email          ${user.email}
         phone          ${user.phone}
         signature      ${user.signature}
         organization_id ${user.organization_id}
         tags           ${user.tags}
         suspended      ${user.suspended}
         organisation_name ${organization.map(_.name).getOrElse("")}
         ${ticketStr}""".stripMargin
    }
  }
  case class TicketSearchResult(
      ticket: Ticket,
      user: Option[User],
      organization: Organization
  ) extends SearchResult {
    override def display(): String = {
      s"""
        Search result:
          _id            ${ticket._id}
          url            ${ticket.url}
          external_id    ${ticket.external_id}
          created_at     ${ticket.created_at}
          `type`            ${ticket.`type`}
          subject           ${ticket.subject}
          description      ${ticket.description}
          priority         ${ticket.priority}
          status           ${ticket.status}
          submitter_id     ${ticket.submitter_id}
          assignee_id      ${ticket.assignee_id}
          organization_id  ${ticket.organization_id}
          tags             ${ticket.tags}
          has_incidents    ${ticket.has_incidents}
          due_at           ${ticket.due_at}
          via           ${ticket.via}
    """
    }
  }
  case class OrganizationSearchResult(
      org: Organization,
      users: List[User],
      tickets: List[Ticket]
  ) extends SearchResult {
    override def display(): String = {
      s""""
        _id                 ${org._id}
        url                 ${org.url}
        external_id         ${org.external_id}
        name                  ${org.name}
        domain_names        ${org.domain_names}
        created_at          ${org.created_at}
        details             ${org.details}
        shared_tickets       ${org.shared_tickets}
        tags                  ${org.tags}
        """
    }
  }
}
