# 🎯 Zenoogle:
Zen desk search cli

## 🖥 Pre Req:
  1. SBT

## 🏃💨 How to run:
```sh
sbt run
```

## How to test:
```bash
sbt test
```

## 🎶 Notes:
  1. The data stored InMemory is aimed at seach optimization.
  2. Searches are O(1) for any fields

## 🌀 Design Decisions:
  1. Using mutable hashmaps to store indexes and other large collection of data as it performs better compared to immutable data structures
  

## 👻 Wild Assumptions:
  1. The CodingChallenge.pdf screenshot has some searchable fields(Tickets.{recipient, requester_id })which doesn't exist in the sample records. assuming these are not a mandatory
 
