package zenoogle
import zenoogle.DataReader.build
import zenoogle.Models.{DataFiles}

class DataReaderSpec extends BaseSpec {
  val data = DataFiles("organizations-limited.json", "users-limited.json", "tickets-limited.json")

  "Given a correct json files to build" should "return datastore" in {

    val (dataStore, links, _) = build(data)

    assert(dataStore.userStore.size == 4)
    assert(dataStore.orgStore.size == 2)
    assert(dataStore.ticketStore.size == 3)

    assert(links.orgTicketIdx.size == 3)
    assert(links.orgUserIdx.size == 4)
    assert(links.orgTicketIdx.size == 3)
  }

  it should "return " in {
    val (_, _, searchMap) = build(data)
    println(searchMap.userIndex)
    assert(searchMap.userIndex.get("emailSome(coffeyrasmussen@flotonic.com)") == Some(List(1)))
  }
}
