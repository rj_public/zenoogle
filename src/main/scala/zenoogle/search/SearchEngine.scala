package zenoogle.search

import zenoogle.Models.{SearchQuery, SearchResult}

trait SearchEngine {
  def search(query: SearchQuery): List[SearchResult]
}
