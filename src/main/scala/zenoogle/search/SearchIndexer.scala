package zenoogle.search

import zenoogle.Models._
import zenoogle.Utils
import scala.collection.mutable

object SearchIndexer {

  def index(
      organizationStore: OrganizationStore,
      userStore: UserStore,
      ticketStore: TicketStore
  ): SearchIndexes = {
    SearchIndexes(
      orgIndex(organizationStore),
      userIndex(userStore),
      ticketIndex(ticketStore)
    )
  }

  def userIndex(users: UserStore): UserSearchIndex = {
    val userIndex: UserSearchIndex = mutable.HashMap.empty

    users.foreach {
      case (userId, user) => {
        val userMap: Map[String, Any] = Utils.caseClassToMap(user)
        val fields = user.searchableFields

        fields.map { key =>
          {
            val hash = key + normalizeValue(userMap.get(key))
            val value = hash -> List(userId) // name+"ironman" -> List(userId)
            userIndex += (value)
          }
        }
      }
    }
    userIndex
  }

  def orgIndex(organizations: OrganizationStore): OrgSearchIndex = {
    val orgIndex: OrgSearchIndex = mutable.HashMap.empty

    organizations.foreach {
      case (id, org) => {
        val rMap: Map[String, Any] = Utils.caseClassToMap(org)
        val fields = org.searchableFields

        fields.map { key =>
          {
            val hash = key + normalizeValue(rMap.get(key))
            val value = hash -> List(id) // name+"ironman" -> List(userId)
            orgIndex += (value)
          }
        }
      }
    }
    orgIndex
  }

  def ticketIndex(tickets: TicketStore): TicketSearchIndex = {
    val ticketIndex: TicketSearchIndex = mutable.HashMap.empty

    tickets.foreach {
      case (id, t) => {
        val rMap: Map[String, Any] = Utils.caseClassToMap(t)
        val fields = t.searchableFields

        fields.map { key =>
          {
            val hash = key + normalizeValue(rMap.get(key))
            val value = hash -> List(id) // name+"ironman" -> List(userId)
            ticketIndex += (value)
          }
        }
      }
    }
    ticketIndex
  }

  def normalizeValue: Any => String = {
    case s: String    => s
    case Some(s: Any) => s.toString()
    case None         => ""
  }

}
